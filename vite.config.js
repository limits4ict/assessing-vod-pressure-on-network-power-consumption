import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import legacy from '@vitejs/plugin-legacy'
import vue2 from '@vitejs/plugin-vue2'

// https://vitejs.dev/config/
export default defineConfig(({mode}) => {
  const env = loadEnv(mode, process.cwd());
  return {
    plugins: [
        vue2(),
        legacy({
        targets: ['ie >= 11'],
        additionalLegacyPolyfills: ['regenerator-runtime/runtime']
        })
    ],
    base: env.VITE_MODE === 'production'
         ? ('/' + env.VITE_CI_PROJECT_NAME + env.VITE_PROJECT_TARGET_PATH + '/')
         : '/',
    resolve: {
        alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    css: {
        preprocessorOptions: {
        scss: {
            includePaths: ["node_modules"]
        }
        }
    }
  }
})

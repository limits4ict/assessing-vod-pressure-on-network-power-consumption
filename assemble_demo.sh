#!/bin/bash

function assemble_npm {
  npm install
  npm ci
  VITE_MODE=production VITE_CI_PROJECT_NAME="$CI_PROJECT_NAME" VITE_PROJECT_TARGET_PATH=$2 npm run build
  ls -lR dist
  mkdir -p $1
  mv dist/* $1/
  ls -lR $1
}

assemble_npm public '/'

ls -lR public

import Vue from 'vue'
import FtthApp from './FTTH-App.vue'
import Buefy from 'buefy'
import './assets/main.scss'

Vue.config.productionTip = false
Vue.use(Buefy, { defaultIconPack: 'fa' })

new Vue({
  render: h => h(FtthApp)
}).$mount('#app')

import * as d3 from "d3-dsv";
import * as parser from "./components/parser.js";
// import { restoreDefault } from "./components/parser.js";

/**
 *  File for utilitary functions
 */
import binomial from '@stdlib/stats/base/dists/binomial'
import betainc from '@stdlib/math-base-special-betainc'

export const utils = {

  /* Object representing couple (bit rate up, bit rate down)*/
  makeUpDown(up, down){
    let  res = {
      up : up,
      down : down, 
    }
    res.onlyUp = () => res.up,
    res.onlyDown = () => res.down,
    res.sum = () => res.up + res.down,
    res.max = ()=> Math.max(res.up,res.down)
    
    res.valueOf = () => (res.up,res.down)
    
    return res
  },

  /**
   * Return function of the form (x)=a+b*x^c such that f(x_i)=y_i for x_i in X and y_i in Y
   * @param {*} X 
   * @param {*} Y 
   * @returns Object of form { a:float, b:float, c:float, f:float => float }
   */
  fitABC(X,Y) {
      let res = {}
      res.a = 1
      res.b = 0
      res.c = -0.5

      for(let i=0; i<1000; ++i) {
        let a0 = res.a
        res.a = i==0 ? 1 : Y[2] - res.b * Math.pow(X[2], res.c)
        let log_b = (Math.log(Y[0]-res.a)*Math.log(X[1]) - Math.log(Y[1]-res.a)*Math.log(X[0])) / (Math.log(X[1])-Math.log(X[0]))
        res.c = (Math.log(Y[0]-res.a)-log_b) / Math.log(X[0])
        res.b = Math.exp(log_b);
        if(Math.abs(a0-res.a)/res.a < 1e-3)
          break
      }

      res.f = function (x) {
        return this.a + this.b * Math.pow(x, this.c)
      }

      return res
  },


  //FIXME UNUSED
  add_bias(percent, amount) {
    return Math.min(percent*amount, 1-(1-percent)/amount)
  },

  bino_cdf(p, k, n) {
    return betainc(1-p, n-k, k+1) / betainc(1,n-k,k+1)
  },

  bino_cdf_inv(p, n, y) {
    let dist = new binomial.Binomial(n, p)
    let k0 = dist.quantile(y)
    let y0 = dist.cdf(k0)
    let k1 = y0<y ? k0+1 : k0-1
    let y1 = dist.cdf(k1)
    return (k1-k0)/(y1-y0)*(y-y0)+k0
  },

      // n -> a+b*n^c
  CorrectionFactorFunc (percent, confidence, K_) {
    if (percent === 0 || percent === 1) {
      return { f: x => 1 }
    }
    let K = K_ // [low mid inf]
    if (!K_) {
      K = [4, 64, 1024]
    }

    // let al = this.bino_cdf_inv(percent, K[0],confidence) / K[0] / percent

    let l = K[0]-1
    let al = l
    do {
      l = l+1
      al = this.bino_cdf_inv(percent,l,confidence)
    } while(Math.round(al) >= l)
    al = al/l/percent;
    K[1] = Math.max(K[1], Math.round(l*1.2))
    K[2] = Math.max(K[2], Math.round(K[1]*1.2))

    let ax = this.bino_cdf_inv(percent, K[1],confidence) / K[1] / percent
    let ainf = this.bino_cdf_inv(percent, K[2],confidence) / K[2] / percent

    let res = {}
    res.abc = this.fitABC([l,K[1],K[2]],[al,ax,ainf])
    res.bound = 1./percent
    res.f = function (x) {
      return Math.min(this.bound, Math.max(1., this.abc.f(x)))
    }

    return res
  },
  /**
   * Calcul number of inhabitabts per home
   * @param {int} nb_sub Number of subscription
   * @returns {int} number of inhabitabts per home
   */
  actual_nb_inhabitants_per_home(nb_sub) {
    let a = 2.1864
    let b = 9.3269
    let c = -0.534
    // FIXME this scaling wrt to changes of the average inhabitants_per_home is very naive:
    return nb_sub * (a + b * Math.pow(nb_sub, c))/2.2*this.methods.get_value('inhabitants_per_home')
  },

  runTests() {
    // tests
    // console.log("TESTS")
    // console.log(utils.bino_cdf(0.8, 4, 32))
    // console.log(utils.bino_cdf(0.5, 100, 200))
    // let N = 400
    // console.log(utils.bino_cdf_inv(0.5, N, 0.99))
    // console.log(utils.bino_cdf_inv(0.5, N, 0.01))
    // console.log(utils.bino_cdf_inv(0.8, N, 0.99))
    // console.log(utils.bino_cdf_inv(0.8, N, 0.01))
    // console.log(utils.bino_cdf_inv(0.2, N, 0.99))
    // console.log(utils.bino_cdf_inv(0.2, N, 0.01))
    // let actual_vod_percent_helper = this.CorrectionFactorFunc(0.12, 0.99, [4,64,128])
    // let actual_vod_percent = function(p,n) {
    //   return p * actual_vod_percent_helper.f(n)
    // }
    // console.log(actual_vod_percent_helper.a, actual_vod_percent_helper.b, actual_vod_percent_helper.c)
    // console.log(actual_vod_percent(0.12,16))
    // console.log(actual_vod_percent(0.12,32))
    // console.log(actual_vod_percent(0.12,64))
    // console.log(actual_vod_percent(0.12,128))

    // let p = 0.12
    // let nits = 1000;
    // let tau = 1.-1e-4;
    // console.log("start 1")
    // let res = 0
    // for (let i =0;i<nits;++i){
    //   res += utils.bino_cdf_inv(p, i+1, tau)/p
    // }
    // console.log("end 1", res)

    // console.log("start 2")
    // res = 0
    // let helper = this.CorrectionFactorFunc(p, tau, [4,64,128])
    // for (let i =0;i<nits;++i){
    //   res += helper.f(i+1)*(i+1)
    // }
    // console.log("end 2", res)
  },

  settings: null,
  references: null,
  registeredDatasets: [],
  
  methods: {

    parseAndRegisterData(rawCsvData, nbList = []) {
      let data = parser.parse(rawCsvData, nbList)
      return this.registerData(data)
    },

    registerData(data) {
      utils.registeredDatasets.push(data)
      return data
    },

    restoreDefault() {
      // console.log(utils.registeredDatasets)
      for(let d of utils.registeredDatasets) {
        // console.log(d)
        parser.restoreData(d, d.backup)
      }
    },

    binary_search (f, v, lohi) {
      let lo = lohi[0]
      let hi = lohi[1]
      let flo = f(lo)
      let fhi = f(hi)
      let dec = flo > fhi ? -1 : 1
      if (v <= flo && v <= fhi) {
        hi = lo
      }
      else if (v >= flo && v >= fhi) {
        lo = hi
      }
      do {
        let mid = Math.round((hi+lo)/2)
        let fmid = f(mid)
        if (dec*fmid < dec*v) {
          lo = mid
        } else {
          hi = mid
        }
      } while (hi-lo>1)
      return [lo, hi]
    },

    cleanupFormula(f) {
      let body = f
        .replace(/_this[0-9]*\./g,'this.')  // cleanup false "this"
        .replace(/[\w]+\.self/g,'this.self')  // cleanup "res.self"
        .replace(/[\n\r]+/g, ' ')           // remove newlines
        .replace(/"/g, '\'')           // cleanup " vs '
        .replace(/function\s*\(\s*\)\s*{\s*return(.*)}\s*/g, "$1") // remove outmost function written in "function () { return ...; }" style"
        .replace(/\(\)\s*=>\s*/g,"")                                   // remove outmost function written in arrow style
      
      // replace nested "function" style to arrow style
      // TODO works only for univariate functions
      let formula_cleaned1 = body
        .replace(/function\s*\(\s*(\w+)\s*\)\s*{\s*return\s*([^;]+);?\s*}/g,"$1 => $2")

      // last cleanup: replace webpack stuff (not needed anymore)
      return formula_cleaned1.replace(/(^|[^_\w])([^\s.()]+\])\./g, "$1this.")
    },
   
    /**
     * Builder of a structure reprensenting a data
     * @param {string} label Name of the data displayed
     * @param {function} formula Function to calcul the data 
     * @param {string} compute_unit Unit used in compute
     * @param {string} display_unit Unit used in display
     * @returns The structure created
     */
    makeValue(label, formula, compute_unit='', display_unit='') {
      let formula_cleaned2 = this.cleanupFormula(formula.toString())

      let res = {
        value: formula(),
        label: label,
        formula: formula_cleaned2,
        formula_func: formula, /* currently unused */
        unit: display_unit,
        computing_unit: compute_unit
      }
      res.valueOf = () => res.value
      return res
    },

    makeFormula(label, formula) {
      let formula_cleaned2 = this.cleanupFormula(formula.toString())      
      let res = {
        label: label,
        formula: formula_cleaned2,
        formula_func: formula, /* currently unused */
      }
      return res
    },

    makeMethod(label, method) {
      let formula_cleaned2 = this.cleanupFormula(method.toString())
      method['label'] = label
      method['formula'] = formula_cleaned2
      method['formula_func'] = method
    },
   
    build_peak_rate_estimator(X = [256,1000,4000]) {
      let res = {}
      res.self = this
      
      const tau = 1-1e-9

      // Baseline
      res.actual_base_percent_helper = utils.CorrectionFactorFunc(this.get_value('active_user_percentage'), tau, X)
      res.actual_base_percent = function(p,n) {
        return p * this.actual_base_percent_helper.f(n)
      }

      res.base = function(nb) {
        return utils.makeUpDown(0, nb * this.actual_base_percent(this.self.get_value('active_user_percentage'), nb) * this.self.get_value('basis_rate'))
      }
      this.makeMethod("base", res.base)

      // VOD
      res.avg_vod_percentage = this.makeValue("",() => res.self.get_value('video_viewer_percentage') * res.self.get_value('vod_viewer_percentage'), "%", "%")
      let actual_vod_percent_helper = utils.CorrectionFactorFunc(res.avg_vod_percentage.value, tau, X)
      let Yvod = X.map(x => actual_vod_percent_helper.f(utils.actual_nb_inhabitants_per_home(x))*utils.actual_nb_inhabitants_per_home(x)/x)
      res.actual_vod_watchers_helper = utils.fitABC(X,Yvod)
      res.actual_vod_watchers_per_sub = function(p,n) {
        return p * this.actual_vod_watchers_helper.f(n)
      }

      res.vod = function(nb) {
        return utils.makeUpDown(0, nb * (this.actual_vod_watchers_per_sub(this.avg_vod_percentage, nb) 
                  / this.self.get_value('viewer_per_device_nb')
                  * this.self.get_value('streaming_rate') ))
      }
      this.makeMethod("vod", res.vod)

      // Large Download
      res.actual_vgdl_percent_helper = utils.CorrectionFactorFunc(this.get_value('vgdl_percentage'), 1-this.get_value('vgdl_confidence'), X)
      res.actual_vgdl_percent = function(p,n) {
        return p * this.actual_vgdl_percent_helper.f(n)
      }

      res.vgdl = function(nb) {
        return utils.makeUpDown(0, nb * this.actual_vgdl_percent(this.self.get_value('vgdl_percentage'), nb) * this.self.get_value('vgdl_rate'))
      }
      this.makeMethod("Large DL", res.vgdl)

      // videocall
      let actual_videocall_percent_helper =utils.CorrectionFactorFunc(this.get_value('videocall_user_percentage'),tau,X)
      let Yvideocall = X.map(x => actual_videocall_percent_helper.f(utils.actual_nb_inhabitants_per_home(x))*utils.actual_nb_inhabitants_per_home(x)/x)
      res.actual_videocall_percent_helper = utils.fitABC(X,Yvideocall)
      res.actual_videocall_percent = function (p,n){
        return p * this.actual_videocall_percent_helper.f(n)
      }

      res.videocall = function(nb) {
        return utils.makeUpDown(
          nb * (this.actual_videocall_percent(this.self.get_value('videocall_user_percentage'),nb)) * this.self.get_value('uploading_videocall_rate'),
          nb * (this.actual_videocall_percent(this.self.get_value('videocall_user_percentage'),nb)) * this.self.get_value('downloading_videocall_rate')
        )
      }
      this.makeMethod("videocall", res.videocall)

      // Summaries

      res.total = function(nb) {
        return  utils.makeUpDown(this.base(nb).up   + this.vod(nb).up   + this.vgdl(nb).up   + this.videocall(nb).up, 
                                 this.base(nb).down + this.vod(nb).down + this.vgdl(nb).down + this.videocall(nb).down)
      }
      res.cdn = function(nb) {
        return utils.makeUpDown(this.vod(nb).up   + this.vgdl(nb).up,
                                this.vod(nb).down + this.vgdl(nb).down)
      }

      return res
    },
    /**
     * Get the value of a parameter in utils.settings
     * @param {*} param Parameter to get the value of
     * @param {*} year optional : column to get the value of. Default : 'value'
     * @returns The value of the parameter
     */
    get_value(param, year = 'value') {
      if (typeof param === 'string' || param instanceof String) {
        return this.unit2ref(this.get_param(param), year)['value'];
      } else {
        return param.value
      }
    },

    get_param(param) {
      if (typeof param === 'string' || param instanceof String) {
        return utils.settings.find((item) => item.name === param);
      } else {
        return param
      }
    },

    /**
     * Get the value if a parameter in any file 
     * @param {*} tab Variable representing the parsed file
     * @param {*} name  The name of parameter you want
     * @param {*} specification  The column you want
     * @returns 
     */
    get_value2(tab, name, specification) {
      let v = tab.find((item) => item.specification === name);
      return this.unit2ref(
        {
          value: v[specification],
          "reference unit": tab.find((item) => item.specification === 'reference unit')[specification],
          unit: tab.find((item) => item.specification === 'unit')[specification],
        },
        "value"
      ).value;
    },


    get_param2(tab, name, specification) {
      let v = tab.find((item) => item.specification === name);
      return {
        value: v[specification],
        "reference unit": tab.find((item) => item.specification === 'reference unit')[specification],
        unit: tab.find((item) => item.specification === 'unit')[specification],
        reference_ids: v["reference_ids"]
      };
    },

    clone_obj(x) {
      var res;
      res = Array.isArray(x) ? [] : {};
      for (var key in x) {
        var v = x[key];
        res[key] = (typeof v === "object") ? this.clone_obj(v) : v;
      }
      return res;
    },

    round(value, precision) {
      var power = Math.pow(10, precision || 0);
      return Math.round(value * power) / power;
    },

    ref2unit(v, key = 'value') {
      let new_value = v[key];
      let new_unit = v.unit;
      switch (v.unit) {
        case "P":
          new_value = v[key] * 1e-15;
          break;
        case "T":
          new_value = v[key] * 1e-12;
          break;
        case "TW":
          new_value = v[key] * 1e-12;
          break;
        case "GW":
          new_value = v[key] * 1e-9;
          break;
        case "MW":
          new_value = v[key] * 1e-6;
          break;
        case "kW":
          new_value = v[key] * 1e-3;
          break;
        case "TWh":
          new_value = v[key] * 1e-12;
          break;
        case "GWh":
          new_value = v[key] * 1e-9;
          break;
        case "MWh":
          new_value = v[key] * 1e-6;
          break;
        case "kWh/GB":
          new_value = v[key] * 1e-3 * 1e9;
          break;
        case "Wh/GB":
          new_value = v[key] * 1e9;
          break;
        case "ZB":
          new_value = v[key] * 1e-21;
          break;
        case "EB":
          new_value = v[key] * 1e-18;
          break;
        case "PB":
          new_value = v[key] * 1e-15;
          break;
        case "TB":
          new_value = v[key] * 1e-12;
          break;
        case "GB":
          new_value = v[key] * 1e-9;
          break;
        case "MB":
          new_value = v[key] * 1e-6;
          break;
        case "GB/hour":
          new_value = v[key] * 1e-9;
          break;
        case "PB/day":
          new_value = v[key] / 1e-15;
          break;
        case "Tb/s":
          new_value = v[key] * 1e-12;
          break;
        case "Gb/s":
          new_value = v[key] * 1e-9;
          break;
        case "Mb/s":
          new_value = v[key] * 1e-6;
          break;
        case "kb/s":
          new_value = v[key] * 1e-3;
          break;
        case "million":
          new_value = v[key] * 1e-6;
          break;
        case "e-6":
          new_value = v[key] * 1e6;
          break;
        case "million hour/day":
          new_value = v[key] * 1e-6;
          break;
        case "hour/year":
          new_value = v[key];
          break;
        case "hour/month":
          new_value = v[key];
          break;
        case "hour/subscription/day":
          new_value = v[key];
          break;
        case "hour":
          new_value = v[key];
          break;
        case "minute":
          new_value = v[key] * 60.;
          break;
        case "cpu hour /hour":
          new_value = v[key];
          break;
        case "cpu hour /GB":
          new_value = v[key] * 1e9;
          break;
        case "exponent":
          new_value = Math.log10(v[key])
          break;
        case "%":
          new_value = v[key]*100
          break;
        case "W":
        case "B":
        case "km":
        case "W/km":
        case "":
        case undefined:
          new_unit = v.unit;
          break;
        default:
          console.log('ref2unit, unknown unit', v.unit)
          new_unit = v.computing_unit;
      }
      return { value: this.round(new_value, 9), unit: new_unit };
    },

    unit2ref(v, key = 'value') {
      let new_value = v[key];
      let new_unit = v.computing_unit;
      switch (v.unit) {
        case "P":
          new_value = v[key] * 1e15;
          break;
        case "T":
          new_value = v[key] * 1e12;
          break;
        case "TW":
          new_value = v[key] * 1e12;
          break;
        case "GW":
          new_value = v[key] * 1e9;
          break;
        case "MW":
          new_value = v[key] * 1e6;
          break;
        case "kW":
          new_value = v[key] * 1e3;
          break;
        case "TWh":
          new_value = v[key] * 1e12;
          break;
        case "GWh":
          new_value = v[key] * 1e9;
          break;
        case "MWh":
          new_value = v[key] * 1e6;
          break;
        case "ZB":
          new_value = v[key] * 1e21;
          break;
        case "EB":
          new_value = v[key] * 1e18;
          break;
        case "PB":
          new_value = v[key] * 1e15;
          break;
        case "TB":
          new_value = v[key] * 1e12;
          break;
        case "GB":
          new_value = v[key] * 1e9;
          break;
        case "MB":
          new_value = v[key] * 1e6;
          break;
        case "kWh/GB":
          new_value = v[key] * 1e3 * 1e-9;
          break;
        case "Wh/GB":
          new_value = v[key] * 1e-9;
          break;
        case "GB/hour":
          new_value = v[key] * 1e9;
          break;
        case "PB/day":
          new_value = v[key] * 1e15;
          break;
        case "million":
          new_value = v[key] * 1e6;
          break;
        case "e-6":
          new_value = v[key] * 1e-6;
          break;
        case "Tb/s":
          new_value = v[key] * 1e12;
          break;
        case "Gb/s":
          new_value = v[key] * 1e9;
          break;
        case "Mb/s":
          new_value = v[key] * 1e6;
          break;
        case "kb/s":
          new_value = v[key] * 1e3;
          break;
        case "million hour/day":
          new_value = v[key] * 1e6;
          break;
        case "hour/month":
          new_value = v[key];
          break;
        case "minute":
          new_value = v[key] / 60.;
          break;
        case "cpu hour /GB":
          new_value = v[key] * 1e-9;
          break;
        case "exponent":
          new_value = Math.pow(10,v[key])
          break;
        case "%":
          new_value = v[key]/100.
          break;
        case "W":
        case "B":
        case "km":
        case "W/km":
        case "hour":
        case "hour/year":
        case "hour/subscription/day":
        case "hour/day":
        case "cpu hour /hour":
        case "":
        case undefined:
          new_unit = v.unit;
          break;
        default:
          console.log('unit2ref, unknown unit', v.unit)
          new_unit = v.unit;
      }
      return { value: new_value, unit: new_unit };
    },
  },
}

var pSBCr = undefined

// https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
export const pSBC=function(p,c0,c1=false,l=false){
    let r,g,b,P,f,t,h,i=parseInt,m=Math.round,a=typeof(c1)=="string";
    if(typeof(p)!="number"||p<-1||p>1||typeof(c0)!="string"||(c0[0]!='r'&&c0[0]!='#')||(c1&&!a))return null;
    if(!pSBCr)pSBCr=(d)=>{
        let n=d.length,x={};
        if(n>9){
            [r,g,b,a]=d=d.split(","),n=d.length;
            if(n<3||n>4)return null;
            x.r=i(r[3]=="a"?r.slice(5):r.slice(4)),x.g=i(g),x.b=i(b),x.a=a?parseFloat(a):-1
        }else{
            if(n==8||n==6||n<4)return null;
            if(n<6)d="#"+d[1]+d[1]+d[2]+d[2]+d[3]+d[3]+(n>4?d[4]+d[4]:"");
            d=i(d.slice(1),16);
            if(n==9||n==5)x.r=d>>24&255,x.g=d>>16&255,x.b=d>>8&255,x.a=m((d&255)/0.255)/1000;
            else x.r=d>>16,x.g=d>>8&255,x.b=d&255,x.a=-1
        }return x};
    h=c0.length>9,h=a?c1.length>9?true:c1=="c"?!h:false:h,f=pSBCr(c0),P=p<0,t=c1&&c1!="c"?pSBCr(c1):P?{r:0,g:0,b:0,a:-1}:{r:255,g:255,b:255,a:-1},p=P?p*-1:p,P=1-p;
    if(!f||!t)return null;
    if(l)r=m(P*f.r+p*t.r),g=m(P*f.g+p*t.g),b=m(P*f.b+p*t.b);
    else r=m((P*f.r**2+p*t.r**2)**0.5),g=m((P*f.g**2+p*t.g**2)**0.5),b=m((P*f.b**2+p*t.b**2)**0.5);
    a=f.a,t=t.a,f=a>=0||t>=0,a=f?a<0?t:t<0?a:a*P+t*p:0;
    if(h)return"rgb"+(f?"a(":"(")+r+","+g+","+b+(f?","+m(a*1000)/1000:"")+")";
    else return"#"+(4294967296+r*16777216+g*65536+b*256+(f?m(a*255):0)).toString(16).slice(1,f?undefined:-2)
}


import * as d3 from "d3-dsv";

var defaultPsv = d3.dsvFormat(",");

export function parse(file, nb_list, psv = defaultPsv) {
  let json_object = psv.parse(file);
  let backup = Array(json_object.length)
  for (let i = 0; i < json_object.length; i++) {
    backup[i] = {}
    for (let j = 0; j < nb_list.length; j++) {
      let val = Number(json_object[i][nb_list[j]])
      json_object[i][nb_list[j]] = val;
      backup[i][nb_list[j]] = val;
    }
  }
  json_object['backup'] = backup
  return json_object;
}

export function restoreData(target, source) {
  for(let i = 0; i < target.length; i++) {
    for(let [key,val] of Object.entries(source[i])) {
      target[i][key] = val
    }
  }
}


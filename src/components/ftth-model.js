import {utils} from "../utils.js"

import settings from "../data/settings.csv?raw"
import oca from "../data/oca.csv?raw"
import aws_instance from "../data/aws_instance.csv?raw"
import references from "../data/references.csv?raw"
import network_devices from "../data/network_devices.csv?raw"

export default {
  props: [],
  mixin: [utils],
  data() {
    return {
      // Datas from csv files
      settings: this.parseAndRegisterData(settings, ["min", "max", "step", "value"]),
      oca_settings: this.parseAndRegisterData(oca),
      aws_instance_settings: this.parseAndRegisterData(aws_instance),
      network_devices: this.parseAndRegisterData(network_devices),
    };
  },

  created() {
    utils.settings = this.settings
    utils.references = this.parseAndRegisterData(references)
  },

  computed: {
    peak_rate_estimator() {
      return this.build_peak_rate_estimator()
    },

    peak_rate_estimator_gpon() {
      return this.build_peak_rate_estimator([48,64,128])
    },
    
    average_peak_rate_per_subscription_base(){
      return this.makeValue(
        "Average peak rate per subscription (base)",
        () => 
        this.get_value('active_user_percentage') *this.get_value('basis_rate'),
        'b/s',
        'Mb/s'
      )
    },  

    average_peak_rate_per_subscription_streaming(){
      return this.makeValue(
        "Average peak rate per subscription (streaming)",
        () => 
        this.get_value('video_viewer_percentage') * this.get_value('vod_viewer_percentage') * this.get_value('streaming_rate') * this.get_value('inhabitants_per_home') / this.get_value('viewer_per_device_nb'),
        'b/s',
        'Mb/s'
      )
    },  

    average_peak_rate_per_subscription_videocall(){
      return this.makeValue(
        "Average peak rate per subscription (videocall)",
        ()=> 
        this.get_value('videocall_user_percentage') * this.get_value('downloading_videocall_rate'),
        'b/s',
        'Mb/s'
      )
    },

    average_peak_rate_per_subscription(){
      return this.makeValue(
        "Average peak rate per subscription",
        () =>
        this.average_peak_rate_per_subscription_base + this.average_peak_rate_per_subscription_streaming + this.average_peak_rate_per_subscription_videocall,
        'b/s',
        'Mb/s'
      )
    },  

    nb_households() {
      return this.makeValue("Number of subscriptions",
       () =>
        Math.ceil(this.get_value('nb_inhabitants') / this.get_value('inhabitants_per_home')),
        '',
       'million')
    },

    // Interesting, but unused
    // total_household_peak_rate(){
    //   return this.makeValue(
    //     "Total downstream bitrate during peak periods",
    //     () => 
    //     this.nb_households * this.average_peak_rate_per_subscription,
    //     'b/s',
    //     'Tb/s'
    //   )
    // },
    
    nb_sub_per_gpon_down() {
      return this.makeValue("Number of subscriptions per GPON ports (downstream)", () => 
          this.binary_search(nb =>
            this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator_gpon.total(nb).down,
            this.get_value2(this.network_devices, 'OLT GPON port', 'capacity'), [1, this.get_value('subscription_per_gpon_tree_nb')])[0],
          '', '')
    },
    nb_sub_per_gpon_up() {
      return this.makeValue("Number of subscriptions per GPON ports (upstream)", () => 
          this.binary_search(nb =>
            this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator_gpon.total(nb).up,
            this.get_value2(this.network_devices, 'OLT GPON port', 'capacity')/2, [1, this.get_value('subscription_per_gpon_tree_nb')])[0],
          '', '')
    },
    nb_sub_per_gpon() {
      return this.makeValue("Number of subscriptions per GPON ports", () => 
        Math.min(this.nb_sub_per_gpon_down, this.nb_sub_per_gpon_up), '', '')
    },
    nb_olt_gpon_ports(){
      return this.makeValue(
        "Number of OLT GPON ports",
        () => 
        Math.ceil( this.nb_households / this.nb_sub_per_gpon + 16 * this.nb_hubs ),
        '',
        ''
      )
    },

    nb_sub_per_olt() {
      return this.makeValue(
        "Number of subscriptions per OLT",
        () => Math.min(this.get_value('nb_subs_per_hub'), this.get_value('max_gpon_port_per_olt')*this.nb_sub_per_gpon), '', '')
    },

    /* FIXME shall we multiply nb by inhabitants_per_home to the baseline needs? */
    /* FIXME actual_vod_percent+actual_base_percent should not be larger than 1 */
    nb_ge_per_olt(){
      return this.makeValue(
        "Number of uplink 10GE ports per OLT",
        () => 
          this.get_value('redundancy_factor') * 
          Math.ceil(this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(this.nb_sub_per_olt).max() / this.get_value2(this.network_devices, 'OLT GE port', 'capacity')),
        '',
        ''
      )
    },

    nb_hubs(){
      return this.makeValue(
        "Total nb of hubs",
        () => Math.ceil(this.nb_households / this.nb_sub_per_olt), '', '')
    },

    nb_olt_ge_ports(){
      return this.makeValue(
        "Number of OLT GE ports",
        () => 
        this.nb_hubs * this.nb_ge_per_olt,
        '',
        ''
      )
    },

    onus_power() {
      return this.makeValue("Home's ONUs", () =>
        this.nb_households * this.get_value2(this.network_devices, 'ONU', 'power'),
        'W', 'kW')
    },

    olts_power() {
      return this.makeValue("Total OLTs power", 
      () =>
        this.get_value('pue_network') * (
            this.nb_olt_gpon_ports * this.get_value2(this.network_devices, 'OLT GPON port', 'power')
          + this.nb_olt_ge_ports * this.get_value2(this.network_devices, 'OLT GE port', 'power')),
        'W', 'kW')
    },

    access_network_power() {
      return this.makeValue("Total access network power", () => this.onus_power + this.olts_power, 'W', 'kW')
    },

    access_network_energy(){
      return this.makeValue(
        "Access",
        () => 
        365 * 24 * this.access_network_power,
        'Wh',
        'GWh'
      )
    },

    level_hub_capacity(){
      return this.makeValue(
        "Capacity need at a hubs->edge link",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(Math.ceil(this.get_value('nb_subs_per_hub'))).sum(),
        'b/s',
        'Gb/s'
      )
    },

    level_edge_capacity(){
      return this.makeValue(
        "Capacity need at a edge->core link",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(Math.ceil(this.nb_households/(8*8*8))).sum(),
        'b/s',
        'Gb/s'
      )
    },

   
    // level3_capacity() {
    //   const formula =
    //   `this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(Math.ceil(this.get_value(this.bordeaux_household_nb)/(8*6*6)))`
    //   return {
    //     label: "Capacity need at a level3->level3 link.",
    //     value: eval(formula),
    //     formula: formula.replace(/[\n\r]+/g, ' '),
    //     unit: "Gb/s",
    //     computing_unit: "b/s",
    //   };
    // },
    level2_capacity(){
      return this.makeValue(
        "Capacity need at a level2->level1 link",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(Math.ceil(this.nb_households/(8*8))).sum(),
        'b/s',
        'Gb/s'
      )
    },
    
    level1_capacity(){
      return this.makeValue(
        "Capacity need at a level1->level0 link",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(Math.ceil(this.nb_households/8)).sum(),
        'b/s',
        'Tb/s'
      )
    },

    level0_capacity(){
      return this.makeValue(
        "Capacity need at level0",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.peak_rate_estimator.total(Math.ceil(this.nb_households)).sum(),
        'b/s',
        'Tb/s'
      )
    },

    // level4_link_power() {
    //   // FIXME this assumes that there is no amplifier
    //   const formula =
    //     `this.get_value(this.nb_hubs) * Math.ceil(this.get_value(this.level4_capacity) / this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'capacity')) * 2 * this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'power')`
    //     return {
    //       label: "Power for hub->level3 links",
    //       value: eval(formula),
    //       formula: formula.replace(/[\n\r]+/g, ' '),
    //       unit: "kW",
    //       computing_unit: "W",
    //     }
    // },

    edge_node_power(){
      return this.makeValue(
        "Power at level3 nodes",
        () => 
        8*8*8 * (
          Math.max(1, this.level_edge_capacity / this.get_value2(this.network_devices, 'edge router', 'capacity')) * this.get_value2(this.network_devices, 'edge router', 'power')
        + Math.max(1, this.level_edge_capacity / this.get_value2(this.network_devices, 'BNG', 'capacity')) * this.get_value2(this.network_devices, 'BNG', 'power')
        + Math.max(1, this.level_edge_capacity / this.get_value2(this.network_devices, 'ethernet switch', 'capacity')) * this.get_value2(this.network_devices, 'ethernet switch', 'power')
        ),
        'W',
        'kW'
      )
    },

    core_node_power(){
      return this.makeValue(
        "Power at level 2 to 0 core nodes",
        () => 
        (
          8*8   * Math.max(1, this.level2_capacity / this.get_value2(this.network_devices, 'core node', 'capacity'))
        + 8     * Math.max(1, this.level1_capacity / this.get_value2(this.network_devices, 'core node', 'capacity'))
        +         Math.max(1, this.level0_capacity / this.get_value2(this.network_devices, 'core node', 'capacity'))
       ) * this.get_value2(this.network_devices, 'core node', 'power'),
        'W',
        'MW'
      )
    },
   
    level_edge_link_power(){
     // FIXME this assumes that there is no amplifier
      return this.makeValue(
        "Power for edge->level2 links",
        () => 
        8*8*8 * Math.ceil(this.level_edge_capacity / this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'capacity'))
        * 2 * this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'power'),
        'W',
        'kW'
      )
    },


    // level3_link_power() {
    //   // FIXME this assumes that there is no amplifier
    //   const formula =
    //     `8*6*6 * Math.ceil(this.get_value(this.level3_capacity) / this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'capacity')) * 2 * this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'power')`
    //     return {
    //       label: "Power for level3->level2 links",
    //       value: eval(formula),
    //       formula: formula.replace(/[\n\r]+/g, ' '),
    //       unit: "kW",
    //       computing_unit: "W",
    //     }
    // },

    level2_link_power(){
       return this.makeValue(
         "Power for level2->level1 links",
         () => 
         8*8 * Math.ceil(this.level2_capacity / this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'capacity')) *
         (  2 * this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'power')
           + (Math.ceil(this.get_value('level2_distance') / this.get_value('terrestrially_amplifier_distance')) - 1) * this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'power') ),
         'W',
         'kW'
       )
     },

    level1_link_power(){
      return this.makeValue(
        "Power for level1->level0 links",
        () => 
        8 * Math.ceil(this.level1_capacity / this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'capacity')) *
         (  2 * this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'power')
          + (Math.ceil(this.get_value('level1_distance') / this.get_value('terrestrially_amplifier_distance')) - 1 ) * this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'power') ),
        'W',
        'kW'
      )
    },

    cumulative_power_per_terrestrial_channel(){
      return this.makeValue(
        "Cumulative power per terrestrial channel",
        () => 
        2 * this.get_value2(this.network_devices, 'terrestrial WDM terminal channel', 'power') + ( Math.ceil(this.get_value('terrestrially_connected_core_router_distance') / this.get_value('terrestrially_amplifier_distance')) - 1 ) * this.get_value2(this.network_devices, 'terrestrial WDM amplifier channel', 'power'),
        'W',
        'W'
      )
    },

    cumulative_power_per_undersea_channel(){
      return this.makeValue(
        "Cumulative power per undersea channel",
        () => 
        2 * this.get_value2(this.network_devices, 'undersea WDM terminal channel', 'power') + 1 / this.get_value('power_feeder_equipment_efficiency') * ( ( Math.ceil(this.get_value('undersea_landing_point_distance') / this.get_value('undersea_repeater_distance')) - 1 ) * this.get_value2(this.network_devices, 'undersea WDM repeater channel', 'power') + this.get_value('undersea_landing_point_distance') * this.get_value('dissipated_power_cable_power_feed') ),
        'W',
        'W'
      )
    },

    core_national_transport_power(){
      return this.makeValue(
        "Core / National (transport)",
        () => 
        this.get_value('redundancy_factor')*this.get_value('pue_network')*(
          this.level_edge_link_power
        + this.level2_link_power
        + this.level1_link_power),
        'W',
        'MW'
      )
    },

    core_national_node_power(){
      return this.makeValue(
        "Core / National (nodes)",
        () => 
        this.get_value('redundancy_factor')*this.get_value('pue_network')*(
          this.edge_node_power
        + this.core_node_power),
        'W',
        'MW'
      )
    },

    core_national_power(){
      return this.makeValue(
        "Core / National",
        () => 
        this.core_national_node_power + this.core_national_transport_power,
        'W',
        'MW'
      )
    },

    cdn_filling_rate(){
      return this.makeValue(
        "CDN filling rate",
        () => 
        this.get_value('storage_server_nb') * this.get_value2(this.oca_settings, 'storage appliance', 'total traffic during fill window') / ( 12. * 3600. ),
        'b/s',
        'Gb/s'
      )
    },

    datacenter_to_IXP_peak_rate(){
      return this.makeValue(
        "Rate between the main DC and the main IXP",
        () => 
        Math.max(this.get_value('stored_content_in_paris_percentage') === 0 ? 0 : this.cdn_filling_rate,
                     this.nb_households * (this.average_peak_rate_per_subscription_base*this.get_value('base_international_percent')
                  + (1-this.get_value('stored_content_in_paris_percentage') ) * this.average_peak_rate_per_subscription_streaming 
                  + this.get_value('videocall_international_percentage') * this.get_value('videocall_user_percentage') *this.get_value('downloading_videocall_rate') )),
        'b/s',
        'Gb/s'
      )
    },

    international_terrestrial_capacity(){
      return this.makeValue(
        "International core network capacity (terrestrial)",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.datacenter_to_IXP_peak_rate,
        'b/s',
        'Tb/s'
      )
    },

    international_undersea_capacity(){
      return this.makeValue(
        "International submarine capacity",
        () => 
        this.get_value('undersea_future_growth_factor') * this.datacenter_to_IXP_peak_rate,
        'b/s',
        'Tb/s'
      )
    },
   
    international_core_node_power(){
      return this.makeValue(
        "Core nodes between the main DC and IXP",
        () => 
        this.get_value('pue_network') * ( this.get_value('nb_international_hops') + 1 )  * this.international_terrestrial_capacity / this.get_value2(this.network_devices, 'core node', 'capacity') * this.get_value2(this.network_devices, 'core node', 'power'),
        'W',
        'kW'
      )
    },

    international_terrestrial_wdm_power(){
      return this.makeValue(
        "Terrestrial WDM systems between the main DC and IXP",
        () => 
        this.get_value('pue_network') * this.get_value('nb_international_hops') * this.international_terrestrial_capacity / this.get_value2(this.network_devices, 'terrestrial channel', 'capacity') * this.cumulative_power_per_terrestrial_channel,
        'W',
        'kW'
      )
    },


    international_terrestrial_core_power(){
      return this.makeValue(
        "Terrestrial cores (international)",
        () => 
        this.get_value('redundancy_factor') * (this.international_core_node_power + this.international_terrestrial_wdm_power),
        'W',
        'kW'
      )
    },

    international_undersea_wdm_power(){
      return this.makeValue(
        "Undersea WDM systems (international)",
        () => 
        this.get_value('pue_network') * this.international_undersea_capacity / this.get_value2(this.network_devices, 'undersea channel', 'capacity') * this.cumulative_power_per_undersea_channel,
        'W',
        'kW'
      )
    },

    cdn_capacity(){
      return this.makeValue(
        "Capacity needs at the CDN",
        () => 
        this.get_value('terrestrial_future_growth_factor') * this.get_value('stored_content_in_paris_percentage') * this.peak_rate_estimator.cdn(Math.ceil(this.nb_households)).sum(),
        'b/s',
        'Tb/s'
      )
    },

    nb_flash_servers(){
      return this.makeValue(
        "Number of flash servers",
        () => 
        Math.ceil(this.cdn_capacity / this.get_value2(this.oca_settings, 'flash appliance', 'operational throughput')),
        '',
        ''
      )
    },
    
    cdn_power(){
      return this.makeValue(
        "CDN",
        () => 
        this.get_value('pue_network') * (
          ( (this.get_value('streaming_rate') === 0 ? 0 : this.get_value('storage_server_nb'))
            * this.get_value2(this.oca_settings, 'storage appliance', 'average power')
            + this.nb_flash_servers * this.get_value2(this.oca_settings, 'flash appliance', 'average power') )
          + this.get_value('redundancy_factor') *
          Math.ceil(this.cdn_capacity / this.get_value2(this.network_devices, 'edge router', 'capacity')) * this.get_value2(this.network_devices, 'edge router', 'power')),
        'W',
        'kW'
      )
    },

    onu_energy(){
      return this.makeValue(
        "Home's ONUs",
        () => 
        this.onus_power * 24 * 365,
        'Wh',
        'GWh'
      )
    },

    DTT_cache_energy(){
      return this.makeValue(
        "Home's DTT caches",
        () => 
        this.nb_households * this.get_value('nb_dtt_cache_per_home') * (
            this.get_value2(this.network_devices, 'DTT cache', 'power')*this.get_value('dtt_cache_hours')
          + this.get_value2(this.network_devices, 'DTT cache', 'Psleep')*(24-this.get_value('dtt_cache_hours')))  * 365,
        'Wh',
        'GWh'
      )
    },

    home_devices_energy(){
      return this.makeValue(
        "Home's ONU & co.",
        () => 
        this.onu_energy + this.DTT_cache_energy,
        'Wh',
        'GWh'
      )
    },

    olt_energy() {
      return this.makeValue("Access", () => this.olts_power * 24 * 365, 'Wh', 'GWh')
    },

    national_core_energy(){
      return this.makeValue(
        "National core",
        () => 
        this.core_national_power * 24 * 365,
        'Wh',
        'GWh'
      )
    },

    international_core_energy() {
      return this.makeValue(
        "International core",
        () =>
        (this.international_terrestrial_core_power + this.international_undersea_wdm_power) * 24 * 365,
        'Wh',
        'GWh'
      )
    },
    
    streaming_volume() {
      return this.makeValue(
        "Streaming volume",
        () =>
        this.nb_households * this.get_value('viewing_hour_nb') * 365 * this.get_value('streaming_rate') / 8 * 3600,
        'B',
        'EB'
      )
    },
    
    baseline_volume() {
      return this.makeValue(
        "Baseline volume",
        () =>
        this.nb_households * 0.0146 * 365 * this.get_value('basis_rate') / 8 * 3600,
        'B',
        'EB'
      )
    },

    vgdl_volume() {
      return this.makeValue(
        "CGDL volume",
        () =>
        this.nb_households * this.get_value('average_yearly_vgdl_volume'),
        'B',
        'EB'
      )
    },

    total_volume() {
      return this.makeValue(
        "Yearly volume",
        () =>
        this.baseline_volume + this.streaming_volume + this.vgdl_volume,
        'B',
        'EB'
      )
    },

    dynamic_streaming_network_energy(){
      return this.makeValue(
        "Dynamic network",
        () =>
        this.total_volume * this.get_value('dynamic_network_intensity'),
        'Wh',
        'GWh'
      )
    },

    cdn_energy(){
      return this.makeValue(
        "CDN",
        () =>
        this.cdn_power * 24 * 365,
        'Wh',
        'GWh'
      )
    },
  },

};

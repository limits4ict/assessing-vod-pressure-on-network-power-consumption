# Assessing VoD pressure on network power consumption - Source Code

This repository hosts the source code of the paper: [Assessing VoD pressure on network power consumption](https://hal.science/hal-04059523) (ICT4S 2023).

This code is provided _as is_ under the [GPLv3 licence](./LICENCE.md).

You can directly access the online demo there : https://limits4ict.gitlabpages.inria.fr/assessing-vod-pressure-on-network-power-consumption/

You may also deploy it locally following the build instructions below.

## Project setup for vuejs

### Install npm dependancies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
